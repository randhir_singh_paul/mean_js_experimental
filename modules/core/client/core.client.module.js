'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('core', ['oc.lazyLoad']);
ApplicationConfiguration.registerModule('core.admin', ['core', 'oc.lazyLoad']);
ApplicationConfiguration.registerModule('core.admin.routes', ['ui.router', 'oc.lazyLoad']);
