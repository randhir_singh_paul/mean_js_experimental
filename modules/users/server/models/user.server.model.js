'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  crypto = require('crypto'),
  validator = require('validator'),
  generatePassword = require('generate-password'),
  owasp = require('owasp-password-strength-test');

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function(property) {
  return ((this.provider !== 'local' && !this.updated) || property.length);
};

/**
 * A Validation function for local strategy email
 */
var validateLocalStrategyEmail = function(email) {
  return ((this.provider !== 'local' && !this.updated) || validator.isEmail(email, {
    require_tld: false
  }));
};

/**
 * User Schema
 */
var UserSchema = new Schema({
  // firstName: {
  //   type: String,
  //   trim: true,
  //   default: '',
  //   validate: [validateLocalStrategyProperty, 'Please fill in your first name']
  // },
  // lastName: {
  //   type: String,
  //   trim: true,
  //   default: '',
  //   validate: [validateLocalStrategyProperty, 'Please fill in your last name']
  // },
  // displayName: {
  //   type: String,
  //   trim: true
  // },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    default: '',
    validate: [validateLocalStrategyEmail, 'Please fill a valid email address']
  },
  username: {
    type: String,
    unique: 'Username already exists',
    required: 'Please fill in a username',
    lowercase: true,
    trim: true
  },
  password: {
    type: String,
    default: ''
  },
  salt: {
    type: String
  },
  profileImageURL: {
    type: String,
    default: 'modules/users/client/img/profile/default.png'
  },
  provider: {
    type: String,
    required: 'Provider is required'
  },
  providerData: {},
  additionalProvidersData: {},
  roles: {
    type: [{
      type: String,
      enum: ['user', 'admin']
    }],
    default: ['user'],
    required: 'Please provide at least one role'
  },
  profile: {
    name: {
      type: String,
      default: ''
    },
    gender: {
      type: String,
      default: ''
    },
    location: {
      type: String,
      default: ''
    },
    website: {
      type: String,
      default: ''
    },
    picture: {
      type: String,
      default: ''
    },
    bgImage: {
      type: String,
      default: ''
    },
    socialUrl: {
      type: String,
      default: ''
    },
    urlType: {
      type: String,
      default: ''
    },
    isMentor: {
      type: Boolean,
      default: false
    },
    recommendations: {
      type: Number,
      default: ''
    },
    quote: {
      type: String,
      default: ''
    },
    featuredVideo: {
      type: String,
      default: ''
    },
  },

  accessAllowed: {
    type: Boolean,
    default: false
  },
  verificationToken: {
    type: String,
    default: ''
  },

  info: {
    finished: {
      type: Boolean,
      default: false
    },
    canceled: {
      type: Boolean,
      default: false
    },
    userType: String, // parent, hs_student, coll_student, professional

    whatToStudy: String,
    particularArea: String,
    colleges: String,
    states: String,
    jobTitle: String,
    company: {
      type: String,
      default: ''
    },
    kids: {
      type: Number,
      default: 0
    },
    //kids details
    kidName1: {
      type: String,
      default: ''
    },
    kidAge1: {
      type: String,
      default: ''
    },
    kidSchool1: {
      type: String,
      default: ''
    },
    kidGPA1: {
      type: String,
      default: ''
    },
    kidSAT1: {
      type: String,
      default: ''
    },
    kidACT1: {
      type: String,
      default: ''
    },
    kidClassGrade1: {
      type: String,
      default: ''
    },
    kid1Study: {
      type: String,
      default: ''
    },
    kid1Career: {
      type: String,
      default: ''
    },
    kid1Image: {
      type: String,
      default: ''
    },
    kid1Scores: [{
      name: {
        type: String,
        default: ''
      },
      act: {
        type: Number,
        default: ''
      },
      sat: {
        type: Number,
        default: ''
      },
      gpa: {
        type: Number,
        default: ''
      }
    }],

    kidName2: {
      type: String,
      default: ''
    },
    kidAge2: {
      type: String,
      default: ''
    },
    kidSchool2: {
      type: String,
      default: ''
    },
    kidGPA2: {
      type: String,
      default: ''
    },
    kidSAT2: {
      type: String,
      default: ''
    },
    kidACT2: {
      type: String,
      default: ''
    },
    kidClassGrade2: {
      type: String,
      default: ''
    },
    kid2Study: {
      type: String,
      default: ''
    },
    kid2Career: {
      type: String,
      default: ''
    },
    kid2Image: {
      type: String,
      default: ''
    },
    kid2Scores: [{
      name: {
        type: String,
        default: ''
      },
      act: {
        type: Number,
        default: ''
      },
      sat: {
        type: Number,
        default: ''
      },
      gpa: {
        type: Number,
        default: ''
      }
    }],

    kidName3: {
      type: String,
      default: ''
    },
    kidAge3: {
      type: String,
      default: ''
    },
    kidSchool3: {
      type: String,
      default: ''
    },
    kidGPA3: {
      type: String,
      default: ''
    },
    kidSAT3: {
      type: String,
      default: ''
    },
    kidACT3: {
      type: String,
      default: ''
    },
    kidClassGrade3: {
      type: String,
      default: ''
    },
    kid3Study: {
      type: String,
      default: ''
    },
    kid3Career: {
      type: String,
      default: ''
    },
    kid3Image: {
      type: String,
      default: ''
    },
    kid3Scores: [{
      name: {
        type: String,
        default: ''
      },
      act: {
        type: Number,
        default: ''
      },
      sat: {
        type: Number,
        default: ''
      },
      gpa: {
        type: Number,
        default: ''
      }
    }],


    kidName4: {
      type: String,
      default: ''
    },
    kidAge4: {
      type: String,
      default: ''
    },
    kidSchool4: {
      type: String,
      default: ''
    },
    kidGPA4: {
      type: String,
      default: ''
    },
    kidSAT4: {
      type: String,
      default: ''
    },
    kidACT4: {
      type: String,
      default: ''
    },
    kidClassGrade4: {
      type: String,
      default: ''
    },
    kid4Study: {
      type: String,
      default: ''
    },
    kid4Career: {
      type: String,
      default: ''
    },
    kid4Image: {
      type: String,
      default: ''
    },
    kid4Scores: [{
      name: {
        type: String,
        default: ''
      },
      act: {
        type: Number,
        default: ''
      },
      sat: {
        type: Number,
        default: ''
      },
      gpa: {
        type: Number,
        default: ''
      }
    }],


    kidName5: {
      type: String,
      default: ''
    },
    kidAge5: {
      type: String,
      default: ''
    },
    kidSchool5: {
      type: String,
      default: ''
    },
    kidGPA5: {
      type: String,
      default: ''
    },
    kidSAT5: {
      type: String,
      default: ''
    },
    kidACT5: {
      type: String,
      default: ''
    },
    kidClassGrade5: {
      type: String,
      default: ''
    },
    kid5Study: {
      type: String,
      default: ''
    },
    kid5Career: {
      type: String,
      default: ''
    },
    kid5Image: {
      type: String,
      default: ''
    },
    kid5Scores: [{
      name: {
        type: String,
        default: ''
      },
      act: {
        type: Number,
        default: ''
      },
      sat: {
        type: Number,
        default: ''
      },
      gpa: {
        type: Number,
        default: ''
      }
    }],


    //hs_student details
    hsHighSchool: {
      type: String,
      default: ''
    },
    hsGPA: {
      type: String,
      default: ''
    },
    hsSAT: {
      type: String,
      default: ''
    },
    hsACT: {
      type: String,
      default: ''
    },
    hsClassGrade: {
      type: String,
      default: false
    },

    race: {
      type: String,
      default: ''
    },
    religion: {
      type: String,
      default: ''
    },
    maritalStatus: {
      type: String,
      default: ''
    },
    sexualOrientation: {
      type: String,
      default: ''
    },
    nationality: {
      type: String,
      default: ''
    },
    parentalStatus: {
      type: String,
      default: ''
    },

    jobtitleCurrent: {
      type: String,
      default: ''
    },
    expertArea: {
      type: String,
      default: ''
    },
    expertAreaKeywords: {
      type: String,
      default: ''
    },

    receiveQueries: {
      type: Boolean,
      default: ''
    },

    onlyMentorValue: {
      type: String,
      default: ''
    },

    myScores: [{
      name: {
        type: String,
        default: ''
      },
      act: {
        type: Number,
        default: ''
      },
      sat: {
        type: Number,
        default: ''
      },
      gpa: {
        type: Number,
        default: ''
      }
    }],
    showAlert: {
      type: Boolean,
      default: true
    },
  },
  education: [{
    university: {
      type: String,
      default: ''
    },
    course: {
      type: String,
      default: ''
    },
    concentration: {
      type: String,
      default: ''
    },
    startDate: {
      type: String,
      default: ''
    },
    endDate: {
      type: String,
      default: ''
    },
    location: {
      type: String,
      default: ''
    },
    GPA: {
      type: String,
      default: ''
    },

  }],
  work: [{
    organisation: {
      type: String,
      default: ''
    },
    position: {
      type: String,
      default: ''
    },
    industry: {
      type: String,
      default: ''
    },
    jobRoles: {
      type: String,
      default: ''
    },
    startDate: {
      type: String,
      default: ''
    },
    endDate: {
      type: String,
      default: ''
    },
    location: {
      type: String,
      default: ''
    },
    salary: {
      type: String,
      default: ''
    },
    jobRecomendation: {
      type: String,
      default: ''
    },
    skillsNeeded: {
      type: String,
      default: ''
    }

  }],
  mySkills: [{
    heading: {
      type: String,
      default: ''
    },
    description: {
      type: String,
      default: ''
    }
  }],
  myAspiration: [{
    title: {
      type: String,
      default: ''
    },
    targetUniversity: {
      type: String,
      default: ''
    },
    targetJob: {
      type: String,
      default: ''
    },
    targetCompany: {
      type: String,
      default: ''
    },
    targetSalary: {
      type: String,
      default: ''
    }
  }],
  mentorProfile: [{
    ethnicity: {
      type: String,
      default: ''
    },
    nationality: {
      type: String,
      default: ''
    },
    preSuccessKids: {
      type: String,
      default: ''
    },
    preSuccessStatus: {
      type: String,
      default: ''
    },
    worstJob: {
      type: String,
      default: ''
    },
  }],
  overAllRecomendation: [{
    name: {
      type: String,
      default: ''
    }
  }],
  updated: {
    type: Date
  },
  created: {
    type: Date,
    default: Date.now
  },
  /* For reset password */
  resetPasswordToken: {
    type: String
  },
  resetPasswordExpires: {
    type: Date
  }
});

/**
 * Hook a pre save method to hash the password
 */
UserSchema.pre('save', function(next) {
  if (this.password && this.isModified('password')) {
    this.salt = crypto.randomBytes(16).toString('base64');
    this.password = this.hashPassword(this.password);
  }

  next();
});

/**
 * Hook a pre validate method to test the local password
 */
UserSchema.pre('validate', function(next) {
  if (this.provider === 'local' && this.password && this.isModified('password')) {
    var result = owasp.test(this.password);
    if (result.errors.length) {
      var error = result.errors.join(' ');
      this.invalidate('password', error);
    }
  }

  next();
});

/**
 * Create instance method for hashing a password
 */
UserSchema.methods.hashPassword = function(password) {
  if (this.salt && password) {
    return crypto.pbkdf2Sync(password, new Buffer(this.salt, 'base64'), 10000, 64).toString('base64');
  } else {
    return password;
  }
};

/**
 * Create instance method for authenticating user
 */
UserSchema.methods.authenticate = function(password) {
  return this.password === this.hashPassword(password);
};

/**
 * Find possible not used username
 */
UserSchema.statics.findUniqueUsername = function(username, suffix, callback) {
  var _this = this;
  var possibleUsername = username.toLowerCase() + (suffix || '');

  _this.findOne({
    username: possibleUsername
  }, function(err, user) {
    if (!err) {
      if (!user) {
        callback(possibleUsername);
      } else {
        return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
      }
    } else {
      callback(null);
    }
  });
};

/**
 * Generates a random passphrase that passes the owasp test
 * Returns a promise that resolves with the generated passphrase, or rejects with an error if something goes wrong.
 * NOTE: Passphrases are only tested against the required owasp strength tests, and not the optional tests.
 */
UserSchema.statics.generateRandomPassphrase = function() {
  return new Promise(function(resolve, reject) {
    var password = '';
    var repeatingCharacters = new RegExp('(.)\\1{2,}', 'g');

    // iterate until the we have a valid passphrase
    // NOTE: Should rarely iterate more than once, but we need this to ensure no repeating characters are present
    while (password.length < 20 || repeatingCharacters.test(password)) {
      // build the random password
      password = generatePassword.generate({
        length: Math.floor(Math.random() * (20)) + 20, // randomize length between 20 and 40 characters
        numbers: true,
        symbols: false,
        uppercase: true,
        excludeSimilarCharacters: true,
      });

      // check if we need to remove any repeating characters
      password = password.replace(repeatingCharacters, '');
    }

    // Send the rejection back if the passphrase fails to pass the strength test
    if (owasp.test(password).errors.length) {
      reject(new Error('An unexpected problem occured while generating the random passphrase'));
    } else {
      // resolve with the validated passphrase
      resolve(password);
    }
  });
};

mongoose.model('User', UserSchema);