'use strict';

// Setting up route
angular.module('users').config(['$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {
    // Users state routing
    $stateProvider
      .state('settings', {
        abstract: true,
        url: '/settings',
        templateUrl: 'modules/users/client/views/settings/settings.client.view.html',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('settings.profile', {
        url: '/profile',
        templateUrl: 'modules/users/client/views/settings/edit-profile.client.view.html',
        data: {
          pageTitle: 'Settings'
        }
      })
      .state('settings.password', {
        url: '/password',
        templateUrl: 'modules/users/client/views/settings/change-password.client.view.html',
        data: {
          pageTitle: 'Settings password'
        }
      })
      .state('settings.accounts', {
        url: '/accounts',
        templateUrl: 'modules/users/client/views/settings/manage-social-accounts.client.view.html',
        data: {
          pageTitle: 'Settings accounts'
        }
      })
      .state('settings.picture', {
        url: '/picture',
        templateUrl: 'modules/users/client/views/settings/change-profile-picture.client.view.html',
        data: {
          pageTitle: 'Settings picture'
        }
      })
      .state('settings.info', {
        url: '/info',
        templateUrl: 'modules/users/client/views/settings/info.client.view.html',
        data: {
          pageTitle: 'Info'
        },
        resolve: {
          loadPlugin: function($ocLazyLoad) {
            return $ocLazyLoad.load([{
              files: [
                '../modules/users/client/css/bootstrap.min.css',
                '../modules/users/client/css/font-awesome.min.css',
                '../modules/users/client/css/animate.min.css',
                // '../modules/users/client/css/jquery.steps.css',
                '../modules/users/client/css/info_functions.css',
                '../modules/users/client/css/chosen/chosen.css',
                '../modules/users/client/css/style.min.css',
                '../modules/users/client/css/styles-core.css',
                '../modules/users/client/css/styles-core-responsive.css',
                '../modules/users/client/css/jquery-ui.css',
                '../modules/users/client/css/tooltipster.css',
                '../modules/users/client/css/sweetalert2.css',

                '../modules/users/client/js/jquery-ui.js',
                '../modules/users/client/js/bootstrap.min.js',
                '../modules/users/client/js/jquery.validate.min.js',
                '../modules/users/client/js/jquery.steps.custom.js',
                '../modules/users/client/js/chosen.jquery.js',
                '../modules/users/client/js/jquery.tooltipster.min.js',
                '../modules/users/client/js/sweetalert2.min.js',
                '../modules/users/client/js/info_functions.js'
              ]
            }]);
          }
        }
      })
      .state('authentication', {
        abstract: true,
        url: '/authentication',
        templateUrl: 'modules/users/client/views/authentication/authentication.client.view.html'
      })
      .state('authentication.signup', {
        url: '/signup',
        templateUrl: 'modules/users/client/views/authentication/signup.client.view.html',
        data: {
          pageTitle: 'Signup'
        },
        resolve: {
          loadPlugin: function($ocLazyLoad) {
            return $ocLazyLoad.load([{
              files: [
                'lib/authentication/style.css',
                'lib/authentication/ui.css',
                '../modules/users/client/css/sweetalert2.css',

                'lib/authentication/js/bootstrap.min.js',
                '../modules/users/client/js/sweetalert2.min.js',
              ]
            }]);
          }
        }
      })
      .state('authentication.signin', {
        url: '/signin?err',
        templateUrl: 'modules/users/client/views/authentication/signin.client.view.html',
        controller: 'AuthenticationController',
        data: {
          pageTitle: 'Signin'
        },
        resolve: {
          loadPlugin: function($ocLazyLoad) {
            return $ocLazyLoad.load([{
              files: [
                'lib/authentication/style.css',
                'lib/authentication/ui.css',
                '../modules/users/client/css/sweetalert2.css',

                'lib/authentication/js/bootstrap.min.js',
                '../modules/users/client/js/sweetalert2.min.js',
              ]
            }]);
          }
        }
      })
      .state('password', {
        abstract: true,
        url: '/password',
        template: '<ui-view/>'
      })
      .state('password.forgot', {
        url: '/forgot',
        templateUrl: 'modules/users/client/views/password/forgot-password.client.view.html',
        data: {
          pageTitle: 'Password forgot'
        }
      })
      .state('password.reset', {
        abstract: true,
        url: '/reset',
        template: '<ui-view/>'
      })
      .state('password.reset.invalid', {
        url: '/invalid',
        templateUrl: 'modules/users/client/views/password/reset-password-invalid.client.view.html',
        data: {
          pageTitle: 'Password reset invalid'
        }
      })
      .state('password.reset.success', {
        url: '/success',
        templateUrl: 'modules/users/client/views/password/reset-password-success.client.view.html',
        data: {
          pageTitle: 'Password reset success'
        }
      })
      .state('password.reset.form', {
        url: '/:token',
        templateUrl: 'modules/users/client/views/password/reset-password.client.view.html',
        data: {
          pageTitle: 'Password reset form'
        }
      });
  }
]);