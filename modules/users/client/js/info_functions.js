var currentProgress = 0;
var userType = null;

function reloadStylesheets() {
   // alert('its working');
    var queryString = '?reload=' + new Date().getTime();
    $('link[rel="stylesheet"]').each(function () {
        console.log('style', $(this));
        this.href = this.href.replace(/\?.*|$/, queryString);
    });

    $('script[type="text/javascript"]').each(function () {
        console.log('script', $(this));
        this.src = this.src.replace(/\?.*|$/, queryString);
    });
}

$(document).ready(function () {
  // setTimeout(function () {
  //   reloadStylesheets();
  // },2000);

    $.ajax({
        url: '/api/users/me',
        dataType: 'json',
        success: function(user) {
            if(user) {
                $('#jobtitleCurrent').val(user.info.jobtitleCurrent);
            }
        },error: function(error) {
            console.log('Error getting user info ', error);
        }
    });

    $('#submitMentor').click(function () {
        var selectedValue = $('input[name=onlyMentor]:checked').val(),
            csrf = $('#_csrf').val();
            if(selectedValue === 'monly') {
                $.ajax({
                    url: '/api/only_mentor',
                    type: 'POST',
                    dataType: 'json',
                    data:{ _csrf: csrf, mentorValue: selectedValue},
                    success: function(response) {
                        window.location.href = '/dashboard#/profile';
                    },error: function (error) {
                        console.log('Error updating the only mentor', error);
                    }
                });        
            }else {
                $('#hiddenMentor').val(selectedValue);
                $('#closeModal').trigger('click');
            }
        
    });

 setTimeout(function () {
   $('a.page-buttons').each(function () {
    if($(this).attr('href') == 'javascript:void(0)') {
        $(this).attr('style', 'display:none');
    }
   });
   $('a.page-buttons').click(function (e) {
     if(currentProgress > 1) {
      //move progress
      if($(this).attr('href') === '#next' && userType) {
       changeProgress(currentProgress, 'next');
      }else if($(this).attr('href') === '#previous') {
       changeProgress(currentProgress, 'previous');
      }
     } 
     return false;
   });
 },1000);


//show the popup where would you like to go
setTimeout(function () {
    $('.finish').click(function () {
        swal({
          title: "All Set",
          text: "Where you want to us to land you ?",
          type: "success",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          cancelButtonClass: "btn-danger",
          confirmButtonText: "Take me to dashboard Feeds",
          cancelButtonText: "I want to explore Steppingblocks",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm) {
          if (isConfirm) {
            //swal("Deleted!", "Your imaginary file has been deleted.", "success");
            $.ajax({
                url: '/info',
                type: 'POST',
                data: $('#form').serialize(),
                success: function(response) {
                    //console.log('form submites ', response);
                    window.location.href = "/dashboard";
                }
            });
            
          } else {
            $.ajax({
                url: '/info',
                type: 'POST',
                data: $('#form').serialize(),
                success: function(response) {
                    //console.log('form submites ', response);
                    window.location.href = "/dashboard#/steppingblocks";
                }
            });
            //
            //swal("Cancelled", "Your imaginary file is safe :)", "error");
          }
        });
    });
});

setTimeout(function () {
 $('.current-tab').click(function (){
      if($(this).attr('id') === 'form-t-0' ) {
        changeProgressTop(1);
      }else if($(this).attr('id') === 'form-t-1' && userType)  {
        changeProgressTop(2);
      }else if($(this).attr('id') === 'form-t-2' && userType)  {
        changeProgressTop(3);
      }else if($(this).attr('id') === 'form-t-3' && userType)  {
        changeProgressTop(4);
      }
   });
 },1000);

$(function() {
 $('.set-gpa').on('focusout', function(e){
  if(parseInt($(this).val()) >= 4 || isNaN(parseInt($(this).val()))) {
   $('.set-sat').focus();
   //$(this).val('4');
  }
 });

 $('.set-sat').on('focusout', function(e){
  if(parseInt($(this).val()) >= 2400 || parseInt($(this).val()) <= 500 || isNaN(parseInt($(this).val()))) {
   $('.set-act').focus();
   // if(parseInt($(this).val()) >= 2400) {
   //  $(this).val('2400');
   // }else{
   //  $(this).val('500');
   // }
  }
 });

 $('.set-act').on('focusout', function(e){
  if(parseInt($(this).val()) >= 36 || isNaN(parseInt($(this).val()))) {
   $('.grade').focus();
   $(this).val('36');
  }
 });

 //kid one
 $('.set-gpak1').on('focusout', function(e){
  if(parseInt($(this).val()) >= 4 || isNaN(parseInt($(this).val()))) {
   $('.set-satk1').focus();
  // $(this).val('4');
  }
 });

 $('.set-satk1').on('focusout', function(e){
  if(parseInt($(this).val()) >= 2400 || parseInt($(this).val()) <= 500 || isNaN(parseInt($(this).val()))) {
   $('.set-actk1').focus();
  //  if(parseInt($(this).val()) >= 2400) {
  //   $(this).val('2400');
  //  }else{
  //   $(this).val('500');
  //  }
  }
 });

 $('.set-actk1').on('focusout', function(e){
  if(parseInt($(this).val()) >= 36 || isNaN(parseInt($(this).val()))) {
   //$('.grade').focus();
  // $(this).val('36');
  }
 });

 //kid two
$('.set-gpak2').on('focusout', function(e){
  if(parseInt($(this).val()) >= 4 || isNaN(parseInt($(this).val()))) {
   $('.set-satk2').focus();
  // $(this).val('4');
  }
 });

 $('.set-satk2').on('focusout', function(e){
  if(parseInt($(this).val()) >= 2400 || parseInt($(this).val()) <= 500 || isNaN(parseInt($(this).val()))) {
   $('.set-actk2').focus();
   // if(parseInt($(this).val()) >= 2400) {
   //  $(this).val('2400');
   // }else{
   //  $(this).val('500');
   // }
  }
 });

 $('.set-actk2').on('focusout', function(e){
  if(parseInt($(this).val()) >= 36 || isNaN(parseInt($(this).val()))) {
   //$('.grade').focus();
 //  $(this).val('36');
  }
 });
 //kid three
$('.set-gpak3').on('focusout', function(e){
  if(parseInt($(this).val()) >= 4 || isNaN(parseInt($(this).val()))) {
   $('.set-satk3').focus();
//   $(this).val('4');
  }
 });

 $('.set-satk3').on('focusout', function(e){
  if(parseInt($(this).val()) >= 2400 || parseInt($(this).val()) <= 500 || isNaN(parseInt($(this).val()))) {
   $('.set-actk3').focus();
 //   if(parseInt($(this).val()) >= 2400) {
 //    $(this).val('2400');
 //   }else{
 //    $(this).val('500');
 //   }
  }
 });

 $('.set-actk3').on('focusout', function(e){
  if(parseInt($(this).val()) >= 36 || isNaN(parseInt($(this).val()))) {
   //$('.grade').focus();
//   $(this).val('36');
  }
 });
 //kid four
$('.set-gpak4').on('focusout', function(e){
  if(parseInt($(this).val()) >= 4 || isNaN(parseInt($(this).val()))) {
   $('.set-sat').focus();
 //  $(this).val('4');
  }
 });

 $('.set-satk4').on('focusout', function(e){
  if(parseInt($(this).val()) >= 2400 || parseInt($(this).val()) <= 500 || isNaN(parseInt($(this).val()))) {
   $('.set-actk4').focus();
 //   if(parseInt($(this).val()) >= 2400) {
 //    $(this).val('2400');
 //   }else{
 //    $(this).val('500');
 //   }
  }
 });

 $('.set-actk4').on('focusout', function(e){
  if(parseInt($(this).val()) >= 36 || isNaN(parseInt($(this).val()))) {
   //$('.grade').focus();
   $(this).val('36');
  }
 });
 //kid five
$('.set-gpak5').on('focusout', function(e){
  if(parseInt($(this).val()) >= 4 || isNaN(parseInt($(this).val()))) {
   $('.set-satk5').focus();
  // $(this).val('4');
  }
 });

 $('.set-satk5').on('focusout', function(e){
  if(parseInt($(this).val()) >= 2400 || parseInt($(this).val()) <= 500 || isNaN(parseInt($(this).val()))) {
   $('.set-actk5').focus();
  //  if(parseInt($(this).val()) >= 2400) {
  //   $(this).val('2400');
  //  }else{
  //   $(this).val('500');
  //  }
  }
 });

 $('.set-actk5').on('focusout', function(e){
  if(parseInt($(this).val()) >= 36 || isNaN(parseInt($(this).val()))) {
  // $('.grade').focus();
   $(this).val('36');
  }
 });
 });
});
function changeProgress(index, status) {
    var $total = 4;
    currentProgress = index;
    var $percent = (currentProgress/$total) * 100;
    var progressBar = $('#form').find('.progress-bar');
    progressBar.css({width:$percent+'%'});
    var regex = new RegExp('\\b' + 'progress-bar-' + '.+?\\b', 'g');
    $('#form').find('.progress-bar')[0].className = progressBar[0].className.replace(regex, '');
    // Progress Bar Color Change - Optional
    if($percent <= 25){
     progressBar.addClass("progress-bar-danger");
    }else if(($percent >= 25) && ($percent <= 50)){
     progressBar.addClass("progress-bar-warning");
    }else if(($percent >= 50) && ($percent <= 75)){
     progressBar.addClass("progress-bar-info");
    }else if(($percent >= 75) && ($percent <= 100)){
     progressBar.addClass("progress-bar-success");
    }else{
     progressBar.addClass("progress-bar-danger");
    }
}
function changeProgressTop(index) {
    var $percent =0 ;
    if(index == 1) {
     $percent = 25;
    }else if(index == 2){
     $percent = 50;
    }else if(index == 3){
     $percent = 75;
    }else if(index == 4){
     $percent = 100;
    }
    currentProgress = index;
    var progressBar = $('#form').find('.progress-bar');
    progressBar.css({width:$percent+'%'});
    var regex = new RegExp('\\b' + 'progress-bar-' + '.+?\\b', 'g');
    $('#form').find('.progress-bar')[0].className = progressBar[0].className.replace(regex, '');
    // Progress Bar Color Change - Optional
    if($percent <= 25){
     progressBar.addClass("progress-bar-danger");
     $('.finish').attr('style', 'display:none');
    }else if(($percent >= 25) && ($percent <= 50)){
     progressBar.addClass("progress-bar-warning");
     $('.finish').attr('style', 'display:none');
    }else if(($percent >= 50) && ($percent <= 75)){
     progressBar.addClass("progress-bar-info");
     $('.finish').attr('style', 'display:none');
    }else if(($percent >= 75) && ($percent <= 100)){
     progressBar.addClass("progress-bar-success");
     $('.finish').attr('style', 'display:block');
    }else{
     progressBar.addClass("progress-bar-danger");
     $('.finish').attr('style', 'display:none');
    }
}
//changeProgress(1, 'next');

var collegeMajorsMajors = '';//JSON.parse('{{collegeMajorsMajors | raw | json}}');
var universityData = '';//{{ universityData | raw | json | safe }};
var states = '';//{{states | raw | json | safe }};
var universityData = '';//{{ universityData | raw | json | safe }};
var natWagesTitles = '';//{{ natWagesTitles | raw | json | safe }};

function bindaAtocomplete(element, array) {
$(element).autocomplete({
minLength: 2,
source: function(request, response) {
var results = $.ui.autocomplete.filter(array, request.term);
response(results.slice(0, 10));
}
}).autocomplete("widget").addClass("white_bg");
}
function bindChangeMajors(parent, child, items) {
$(parent).on('change', function(){
var selectedId = $(this).val();
if (selectedId === '') {
$(child).html('').prop('disabled', true);
} else {
$(child).html('').prop('disabled', false);
$('<option></option>').val('').text('').appendTo($(child));
$.each(items, function(index, item){
if (item.coll_id == selectedId) {
$('<option></option>').val(item.major_id).text(item.major).appendTo($(child));
}
});
}
});
}

// first kid
function kidStudy1Changed(value) {
if(value){
$.ajax({
  url:'/api/postgresql/edu_drill_two?filters={"coll_id":"' + value + '"}',
  dataType: 'json',
  success: function(response){
      var options = '';
      if(response) {
          $.each(response, function(key,value){
              options = options + '<option value='+ value.major_id +'>'+ value.major +'</option>';
          });
      }
      $('#particularArea1KidOne').append(options);
      $('#particularArea1KidOne').prop('disabled', false);
      $("#particularArea1KidOne option:eq(0)").prop("selected", true);
      $("#particularArea2KidOne option:eq(0)").prop("selected", true);
      $("#particularArea2KidOne").prop("disabled", true);
  },error: function(error) {
      console.log('Error ', error);
  }
});
}
}

// function setHsDesiredStudy(value) {
//     console.log('Here ', value, value);
//     //hsStudtTitleHidden
// }

function particularArea1KidOneChanged(value) {
if(value) {
$.ajax({
  url:'/api/postgresql/edu_drill_three?filters={"major_id":"' + value + '"}',
  dataType: 'json',
  success: function(response){
      var options = '';
      if(response) {
          $.each(response, function(key,value){
              options = options + '<option value='+ value.focus_id +'>'+ value.focus +'</option>';
          });
      }
      $('#particularArea2KidOne').append(options);
      $('#particularArea2KidOne').prop('disabled', false);
      $("#particularArea2KidOne option:eq(0)").prop("selected", true);
  },error: function(error) {
      console.log('Error ', error);
  }
});
}
}

//second kid
function kidStudy2Changed(value) {
if(value){
$.ajax({
  url:'/api/postgresql/edu_drill_two?filters={"coll_id":"' + value + '"}',
  dataType: 'json',
  success: function(response){
      var options = '';
      if(response) {
          $.each(response, function(key,value){
              options = options + '<option value='+ value.major_id +'>'+ value.major +'</option>';
          });
      }
      $('#particularArea1KidTwo').append(options);
      $('#particularArea1KidTwo').prop('disabled', false);
      $("#particularArea1KidTwo option:eq(0)").prop("selected", true);
      $("#particularArea2KidTwo option:eq(0)").prop("selected", true);
      $("#particularArea2KidTwo").prop("disabled", true);
  },error: function(error) {
      console.log('Error ', error);
  }
});
}
}

function particularArea1KidTwoChanged(value) {
if(value) {
$.ajax({
  url:'/api/postgresql/edu_drill_three?filters={"major_id":"' + value + '"}',
  dataType: 'json',
  success: function(response){
      var options = '';
      if(response) {
          $.each(response, function(key,value){
              options = options + '<option value='+ value.focus_id +'>'+ value.focus +'</option>';
          });
      }
      $('#particularArea2KidTwo').append(options);
      $('#particularArea2KidTwo').prop('disabled', false);
      $("#particularArea2KidTwo option:eq(0)").prop("selected", true);
  },error: function(error) {
      console.log('Error ', error);
  }
});
}
}

//third kid
function kidStudy3Changed(value) {
if(value){
$.ajax({
  url:'/api/postgresql/edu_drill_two?filters={"coll_id":"' + value + '"}',
  dataType: 'json',
  success: function(response){
      var options = '';
      if(response) {
          $.each(response, function(key,value){
              options = options + '<option value='+ value.major_id +'>'+ value.major +'</option>';
          });
      }
      $('#particularArea1KidThree').append(options);
      $('#particularArea1KidThree').prop('disabled', false);
      $("#particularArea1KidThree option:eq(0)").prop("selected", true);
      $("#particularArea2KidThree option:eq(0)").prop("selected", true);
      $("#particularArea2KidThree").prop("disabled", true);
  },error: function(error) {
      console.log('Error ', error);
  }
});
}
}

function particularArea1KidThreeChanged(value) {
if(value) {
$.ajax({
  url:'/api/postgresql/edu_drill_three?filters={"major_id":"' + value + '"}',
  dataType: 'json',
  success: function(response){
      var options = '';
      if(response) {
          $.each(response, function(key,value){
              options = options + '<option value='+ value.focus_id +'>'+ value.focus +'</option>';
          });
      }
      $('#particularArea2KidThree').append(options);
      $('#particularArea2KidThree').prop('disabled', false);
      $("#particularArea2KidThree option:eq(0)").prop("selected", true);
  },error: function(error) {
      console.log('Error ', error);
  }
});
}
}

//kid four
function kidStudy4Changed(value) {
if(value){
$.ajax({
  url:'/api/postgresql/edu_drill_two?filters={"coll_id":"' + value + '"}',
  dataType: 'json',
  success: function(response){
      var options = '';
      if(response) {
          $.each(response, function(key,value){
              options = options + '<option value='+ value.major_id +'>'+ value.major +'</option>';
          });
      }
      $('#particularArea1KidFour').append(options);
      $('#particularArea1KidFour').prop('disabled', false);
      $("#particularArea1KidFour option:eq(0)").prop("selected", true);
      $("#particularArea2KidFour option:eq(0)").prop("selected", true);
      $("#particularArea2KidFour").prop("disabled", true);
  },error: function(error) {
      console.log('Error ', error);
  }
});
}
}

function particularArea1KidFourChanged(value) {
if(value) {
$.ajax({
  url:'/api/postgresql/edu_drill_three?filters={"major_id":"' + value + '"}',
  dataType: 'json',
  success: function(response){
      var options = '';
      if(response) {
          $.each(response, function(key,value){
              options = options + '<option value='+ value.focus_id +'>'+ value.focus +'</option>';
          });
      }
      $('#particularArea2KidFour').append(options);
      $('#particularArea2KidFour').prop('disabled', false);
      $("#particularArea2KidFour option:eq(0)").prop("selected", true);
  },error: function(error) {
      console.log('Error ', error);
  }
});
}
}

//kid five
function kidStudy5Changed(value) {
if(value){
$.ajax({
  url:'/api/postgresql/edu_drill_two?filters={"coll_id":"' + value + '"}',
  dataType: 'json',
  success: function(response){
      var options = '';
      if(response) {
          $.each(response, function(key,value){
              options = options + '<option value='+ value.major_id +'>'+ value.major +'</option>';
          });
      }
      $('#particularArea1KidFive').append(options);
      $('#particularArea1KidFive').prop('disabled', false);
      $("#particularArea1KidFive option:eq(0)").prop("selected", true);
      $("#particularArea2KidFive option:eq(0)").prop("selected", true);
      $("#particularArea2KidFive").prop("disabled", true);
  },error: function(error) {
      console.log('Error ', error);
  }
});
}
}

function particularArea1KidFiveChanged(value) {
if(value) {
$.ajax({
  url:'/api/postgresql/edu_drill_three?filters={"major_id":"' + value + '"}',
  dataType: 'json',
  success: function(response){
      var options = '';
      if(response) {
          $.each(response, function(key,value){
              options = options + '<option value='+ value.focus_id +'>'+ value.focus +'</option>';
          });
      }
      $('#particularArea2KidFive').append(options);
      $('#particularArea2KidFive').prop('disabled', false);
      $("#particularArea2KidFive option:eq(0)").prop("selected", true);
  },error: function(error) {
      console.log('Error ', error);
  }
});
}
}

function collegeMajorsColleges2Changed(value) {
if(value){
$.ajax({
  url:'/api/postgresql/edu_drill_two?filters={"coll_id":"' + value + '"}',
  dataType: 'json',
  success: function(response){
      var options = '';
      if(response) {
          $.each(response, function(key,value){
              options = options + '<option value='+ value.major_id +'>'+ value.major +'</option>';
          });
      }
      $('#particularAreaHs2').append(options);
      $('#particularAreaHs2').prop('disabled', false);
      $("#particularAreaHs2 option:eq(0)").prop("selected", true);
      $("#hsStudtTitle option:eq(0)").prop("selected", true);
      $("#hsStudtTitle").prop("disabled", true);
  },error: function(error) {
      console.log('Error ', error);
  }
});
}            
}

function collegeMajorsColleges3Changed(value) {
if(value){
$.ajax({
  url:'/api/postgresql/edu_drill_two?filters={"coll_id":"' + value + '"}',
  dataType: 'json',
  success: function(response){
      var options = '';
      if(response) {
          $.each(response, function(key,value){
              options = options + '<option value='+ value.major_id +'>'+ value.major +'</option>';
          });
      }
      $('#particularArea3P').append(options);
      $('#particularArea3P').prop('disabled', false);
      $("#particularArea3P option:eq(0)").prop("selected", true);
  },error: function(error) {
      console.log('Error ', error);
  }
});
} 
}

function particularAreaHs2Changed(value) {
//console.log('Here ', value);
if(value) {
$.ajax({
  url:'/api/postgresql/edu_drill_three?filters={"major_id":"' + value + '"}',
  dataType: 'json',
  success: function(response){
      var options = '';
      if(response) {
          $.each(response, function(key,value){
              options = options + '<option value='+ value.focus_id +'>'+ value.focus +'</option>';
          });
      }
      $('#hsStudtTitle').append(options);
      $('#hsStudtTitle').prop('disabled', false);
      $("#hsStudtTitle option:eq(0)").prop("selected", true);
  },error: function(error) {
      console.log('Error ', error);
  }
});
}
}

function changedCollegeThird(value) {
if(value) {
$.ajax({
  url:'/api/postgresql/edu_drill_three?filters={"major_id":"' + value + '"}',
  dataType: 'json',
  success: function(response){
      var options = '';
      if(response) {
          $.each(response, function(key,value){
              options = options + '<option value='+ value.focus_id +'>'+ value.focus +'</option>';
          });
      }
      $('#collparticularArea3').append(options);
      $('#collparticularArea3').prop('disabled', false);
      $("#collparticularArea3 option:eq(0)").prop("selected", true);
  },error: function(error) {
      console.log('Error ', error);
  }
});
}
}

function hsHighSchoolLocChanged(value) {
if(value) {
$.ajax({
  url:'/api/postgresql/get_schools?filters={"state":"' + value + '"}',
  dataType: 'json',
  success: function(response) {
      var schoolsArray = [];
      $.map(response, function(item) {
          schoolsArray.push(item.school);
      });
      bindaAtocomplete("#hsHighSchool", schoolsArray);
      bindaAtocomplete("#kidSchool1", schoolsArray);
      bindaAtocomplete("#kidSchool2", schoolsArray);
      bindaAtocomplete("#kidSchool3", schoolsArray);
      bindaAtocomplete("#kidSchool4", schoolsArray);
      bindaAtocomplete("#kidSchool5", schoolsArray);
      $('#hsHighSchool').prop('disabled', false);
  },error: function(error) {  
      console.log('Error loading schools list', error);
  }
});
}
}

function pHighSchoolLocChanged(value) { 
if(value) {
$.ajax({
  url:'/api/postgresql/get_schools?filters={"state":"' + value + '"}',
  dataType: 'json',
  success: function(response) {
      var schoolsArray = [];
      $.map(response, function(item) {
          schoolsArray.push(item.school);
      });
      bindaAtocomplete("#pHighSchool", schoolsArray);
      $('#pHighSchool').prop('disabled', false);
  },error: function(error) {  
      console.log('Error loading schools list', error);
  }
});
}
}

function bindMainButtons() {
$(".widget").click(function() {
userType = $(this).data('type');
console.log('user type', userType);
if(userType === 'professional') {
    $('#onlyMentorBtn').trigger('click');
    //return false
}
$('#userType').val(userType);
$('.step2').hide();
$('.' + userType).show();
$('#form').steps('next');
});
}
$(document).ready(function () {
$("#form").steps({
bodyTag: "fieldset",
cssClass: 'wizard steppingbloks',
transitionEffect: "slideLeft",
enableCancelButton: true,
onCanceled: function (event, currentIndex, newIndex) {
$("#canceled").val(true);
$("#form").submit();
},
onStepChanging: function (event, currentIndex, newIndex) {
if (!userType) {
return false;
}
return true;
},
onFinishing: function (event, currentIndex) {
return true;
},
onFinished: function (event, currentIndex) {
$("#form").submit();
}
});
bindMainButtons();
bindaAtocomplete("#colleges1", universityData);
bindaAtocomplete("#colleges2", universityData);
bindaAtocomplete("#colleges3", universityData);
bindaAtocomplete("#states1", states);
bindaAtocomplete("#states2", states);
bindaAtocomplete("#states3", states);
//bindaAtocomplete("#hsHighSchoolLoc", states);
bindaAtocomplete("#jobtitle", natWagesTitles);
bindaAtocomplete(".open-search", natWagesTitles);
bindaAtocomplete("#collCurrentStudy", natWagesTitles);
bindaAtocomplete("#collDesiredStudy", natWagesTitles);

// bindChangeMajors('#collegeMajorsColleges1', '#particularArea1', collegeMajorsMajors);
bindChangeMajors('#collegeMajorsColleges2', '#particularArea2', collegeMajorsMajors);
bindChangeMajors('#collegeMajorsColleges3', '#particularArea3', collegeMajorsMajors);

$('div.div_kd').slideUp('slow');

//change kid names
$('#kidName1').focusout(function (){ 
if($(this).val() !=='' && $(this).val() != undefined) {                
$('#kid1name').text($(this).val());
}
});
$('#kidName2').focusout(function (){ 
if($(this).val() !=='' && $(this).val() != undefined) {                
$('#kid2name').text($(this).val());
}
});
$('#kidName3').focusout(function (){ 
if($(this).val() !=='' && $(this).val() != undefined) {                
$('#kid3name').text($(this).val());
}
});
$('#kidName4').focusout(function (){ 
if($(this).val() !=='' && $(this).val() != undefined) {                
$('#kid4name').text($(this).val());
}
});
$('#kidName5').focusout(function (){ 
if($(this).val() !=='' && $(this).val() != undefined) {                
$('#kid5name').text($(this).val());
}
});

//hide the filled forms
$('#hideKidOne').click(function () {
var currentH = $('.content.clearfix').height();
var newH = currentH - 590;
$('#1').slideUp('slow');
$('.content.clearfix').height(newH + 'px');
});
$('#hideKidTwo').click(function () {
var currentH = $('.content.clearfix').height();
var newH = currentH - 590;
$('.content.clearfix').height(newH + 'px');
$('#2').slideUp('slow');
});
$('#hideKidThree').click(function () {
var currentH = $('.content.clearfix').height();
var newH = currentH - 590;
$('.content.clearfix').height(newH + 'px');
$('#3').slideUp('slow');
});
$('#hideKidFour').click(function () {
var currentH = $('.content.clearfix').height();
var newH = currentH - 590;
$('.content.clearfix').height(newH + 'px');
$('#4').slideUp('slow');
});
$('#hideKidFive').click(function () {
var currentH = $('.content.clearfix').height();
var newH = currentH - 590;
$('.content.clearfix').height(newH + 'px');
$('#5').slideUp('slow');
});

$('#hsStudtTitle').change(function (){
var newValue = $(this).find("option:selected").text();
$('#hsStudtTitleHidden').val(newValue);
});

$('#particularArea1KidOne').change(function (){
var newValue = $(this).find("option:selected").text();
$('#kid1Study').val(newValue);
});

$('#particularArea2KidOne').change(function (){
var newValue = $(this).find("option:selected").text();
$('#kid2Study').val(newValue);
});

$('#particularArea2KidOne').change(function (){
var newValue = $(this).find("option:selected").text();
$('#kid3Study').val(newValue);
});

$('#particularArea2KidOne').change(function (){
var newValue = $(this).find("option:selected").text();
$('#kid4Study').val(newValue);
});

$('#particularArea2KidOne').change(function (){
var newValue = $(this).find("option:selected").text();
$('#kid5Study').val(newValue);
});

$('#collparticularArea3').change(function (){
var newValue = $(this).find("option:selected").text();
$('#collparticularArea3Hidden').val(newValue);
});

$('#particularArea3P').change(function (){
var newValue = $(this).find("option:selected").text();
$('#particularArea3PHidden').val(newValue);
});        

});

function changeKids(value){
    var windowsize = $(window).width();
    console.log('now the window is ', windowsize);
    
    if(value == 1) {
        $('.content.clearfix').height('950px');
    }else if(value == 2) {
        $('.content.clearfix').height('1530px');
    }else if(value == 3) {
        $('.content.clearfix').height('2098px');
    }else if(value == 4) {
        $('.content.clearfix').height('2673px');
    }else if(value == 5) {
        $('.content.clearfix').height('3250px');
    }else{
        $('.content.clearfix').height('375px');
    }

    //for the mobile browsers only
    if((windowsize == 320 || windowsize == 340 || windowsize == 768) && value == 1) {
        $('.content.clearfix').height('990px');
    }else if((windowsize == 320 || windowsize == 340 || windowsize == 768) && value == 2) {
        $('.content.clearfix').height('1700px');        
    }else if((windowsize == 320 || windowsize == 340 || windowsize == 768) && value == 3) {
        $('.content.clearfix').height('2400px');        
    }else if((windowsize == 320 || windowsize == 340 || windowsize == 768) && value == 4) {
        $('.content.clearfix').height('3110px');        
    }else if((windowsize == 320 || windowsize == 340 || windowsize == 768) && value == 5) {
        $('.content.clearfix').height('3830px');        
    }

    $( "div.div_kd" ).each(function( index ) {     
        if($( this ).attr('id') <= value) {       
            $( this ).slideDown('slow');
        }else{
            $( this ).slideUp('slow');
        }
    });
}

function showMentorSchool(value) { console.log(value);
if(value === '1') {
$('.hideMentorSchool').show('slow');
$('.content.clearfix').height('825px');
}else{
$('.hideMentorSchool').hide('slow');
$('.content.clearfix').height('687px');
}
}

  $(function () {
        $('#datetimepickerKid1').datepicker({ dateFormat: 'mm/dd/yy' });
        $('#datetimepickerKid2').datepicker({ dateFormat: 'mm/dd/yy' });
        $('#datetimepickerKid3').datepicker({ dateFormat: 'mm/dd/yy' });
        $('#datetimepickerKid4').datepicker({ dateFormat: 'mm/dd/yy' });
        $('#datetimepickerKid5').datepicker({ dateFormat: 'mm/dd/yy' });

        // $('#demo-icon').tooltipster({
        //     iconDesktop: true,
        //     iconTouch: true
        // });

    });

     $(document).ready(function() {
            $('.tooltip').tooltipster();
        });
