(function (app) {
  'use strict';

  app.registerModule('dashboards');
  app.registerModule('dashboards.services');
  app.registerModule('dashboards.routes', ['ui.router', 'dashboards.services']);
})(ApplicationConfiguration);
