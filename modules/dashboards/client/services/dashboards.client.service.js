(function () {
  'use strict';

  angular
    .module('dashboards.services')
    .factory('DashboardsService', DashboardsService);

  DashboardsService.$inject = ['$resource'];

  function DashboardsService($resource) {
    return $resource('api/dashboards/:dashboardId', {
      dashboardId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
})();
