'use strict';

/**
 * Module dependencies
 */
var tutionsPolicy = require('../policies/tutions.server.policy'),
  tutions = require('../controllers/tutions.server.controller');

module.exports = function (app) {

  //Get All States
  //app.route('/api/tution_states').all(tutionsPolicy.isAllowed)
  app.route('/api/tution_states').get(tutions.states_list);

  // Articles collection routes
  app.route('/api/tutions').all(tutionsPolicy.isAllowed)
    .get(tutions.list)
    .post(tutions.create);

  // Single tution routes
  app.route('/api/tutions/:tutionId').all(tutionsPolicy.isAllowed)
    .get(tutions.read)
    .put(tutions.update)
    .delete(tutions.delete);

  // Finish by binding the tution middleware
  app.param('tutionId', tutions.tutionByID);
};
