'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Tution = mongoose.model('Tution'),
  fs = require('fs'),
  path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));


/**
 * Get The List Of States From JSON File
 */
exports.states_list = function(req, res) {

  fs.readFile(path.resolve(__dirname, '../jsonData/states_titlecase.json'), 'UTF-8', function(err, data) {
    if (err) {
      console.log(err);
      return;
    }
    return res.send(data);
  });
}

/**
 * Create an tution
 */
exports.create = function(req, res) {
  var tution = new Tution(req.body);
  tution.user = req.user;

  tution.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(tution);
    }
  });
};

/**
 * Show the current tution
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var tution = req.tution ? req.tution.toJSON() : {};

  // Add a custom field to the Tution, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Tution model.
  tution.isCurrentUserOwner = req.user && tution.user && tution.user._id.toString() === req.user._id.toString() ? true : false;

  res.json(tution);
};

/**
 * Update an tution
 */
exports.update = function(req, res) {
  var tution = req.tution;

  tution.title = req.body.title;
  tution.content = req.body.content;

  tution.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(tution);
    }
  });
};

/**
 * Delete an tution
 */
exports.delete = function(req, res) {
  var tution = req.tution;

  tution.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(tution);
    }
  });
};

/**
 * List of Tutions
 */
exports.list = function(req, res) {
  Tution.find().sort('-created').populate('user', 'displayName').exec(function(err, tutions) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(tutions);
    }
  });
};

/**
 * Tution middleware
 */
exports.tutionByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Tution is invalid'
    });
  }

  Tution.findById(id).populate('user', 'displayName').exec(function(err, tution) {
    if (err) {
      return next(err);
    } else if (!tution) {
      return res.status(404).send({
        message: 'No tution with that identifier has been found'
      });
    }
    req.tution = tution;
    next();
  });
};