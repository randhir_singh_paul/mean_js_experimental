(function (app) {
  'use strict';

  app.registerModule('tutions');
  app.registerModule('tutions.services');
  app.registerModule('tutions.routes', ['ui.router', 'tutions.services']);
})(ApplicationConfiguration);
