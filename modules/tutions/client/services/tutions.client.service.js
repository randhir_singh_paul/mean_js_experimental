(function() {
  'use strict';

  angular
    .module('tutions.services')
    .factory('TutionsService', TutionsService);

  TutionsService.$inject = ['$resource', '$http'];

  function TutionsService($resource, $http) {

    var factory = {};
    factory.getStates = function() {
      $http.get('/api/tution_states').
      success(function(data, status) {
        return data;
      }).
      error(function(data, status) {

      });
    };
    return factory;
  }
})();