(function() {
  'use strict';

  angular
    .module('tutions')
    .controller('TutionsListController', TutionsListController);

  TutionsListController.$inject = ['$scope', '$state', '$http', 'Authentication', 'TutionsService'];

  function TutionsListController($scope, $state, $http, Authentication, TutionsService) {
    var vm = this;

    //$scope.states = TutionsService.getStates();
    $scope.getStates = function() {
      $http.get('/api/tution_states').
      success(function(data, status) {
        // return data;
        $scope.states = data;
      }).
      error(function(data, status) {

      });
    }

    $scope.getUniversities = function() {

      //$scope.hideSpinner = false;
      // PostgreService.get_tution_unis($scope.tution.state).then(function(rows) {
      //   //Sort The Array
      //   rows.sort(function(a, b) {
      //     var alc = a.institution_name.toLowerCase(),
      //       blc = b.institution_name.toLowerCase();
      //     return alc > blc ? 1 : alc < blc ? -1 : 0;
      //   });

      //   $scope.unis = rows;
      //   $scope.hideSpinner = true;
      // });
    }
  }
})();