(function () {
  'use strict';

  angular
    .module('tutions')
    .controller('TutionsController', TutionsController);

  TutionsController.$inject = ['$scope', '$state', 'tutionResolve', 'Authentication'];

  function TutionsController($scope, $state, tution, Authentication) {
    var vm = this;

    vm.tution = tution;
    vm.authentication = Authentication;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Tution
    function remove() {
      if (confirm('Are you sure you want to delete?')) {
        vm.tution.$remove($state.go('tutions.list'));
      }
    }

    // Save Tution
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.tutionForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.tution._id) {
        vm.tution.$update(successCallback, errorCallback);
      } else {
        vm.tution.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('tutions.view', {
          tutionId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
})();
