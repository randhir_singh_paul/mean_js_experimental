(function () {
  'use strict';

  angular
    .module('tutions.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('tutions', {
        abstract: true,
        url: '/tutions',
        template: '<ui-view/>'
      })
      .state('tutions.list', {
        url: '',
        templateUrl: 'modules/tutions/client/views/list-tutions.client.view.html',
        controller: 'TutionsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Tutions List'
        }
      })
      .state('tutions.create', {
        url: '/create',
        templateUrl: 'modules/tutions/client/views/form-tution.client.view.html',
        controller: 'TutionsController',
        controllerAs: 'vm',
        resolve: {
          tutionResolve: newTution
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle : 'Tutions Create'
        }
      })
      .state('tutions.edit', {
        url: '/:tutionId/edit',
        templateUrl: 'modules/tutions/client/views/form-tution.client.view.html',
        controller: 'TutionsController',
        controllerAs: 'vm',
        resolve: {
          tutionResolve: getTution
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Tution {{ tutionResolve.title }}'
        }
      })
      .state('tutions.view', {
        url: '/:tutionId',
        templateUrl: 'modules/tutions/client/views/view-tution.client.view.html',
        controller: 'TutionsController',
        controllerAs: 'vm',
        resolve: {
          tutionResolve: getTution
        },
        data:{
          pageTitle: 'Tution {{ tutionResolve.title }}'
        }
      });
  }

  getTution.$inject = ['$stateParams', 'TutionsService'];

  function getTution($stateParams, TutionsService) {
    return TutionsService.get({
      tutionId: $stateParams.tutionId
    }).$promise;
  }

  newTution.$inject = ['TutionsService'];

  function newTution(TutionsService) {
    return new TutionsService();
  }
})();
