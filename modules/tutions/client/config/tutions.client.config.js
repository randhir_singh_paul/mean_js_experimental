(function () {
  'use strict';

  angular
    .module('tutions')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    Menus.addMenuItem('topbar', {
      title: 'Tutions',
      state: 'tutions',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'tutions', {
      title: 'List Tution',
      state: 'tutions.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'tutions', {
      title: 'Create Tution',
      state: 'tutions.create',
      roles: ['user']
    });
  }
})();
