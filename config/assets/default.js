'use strict';

module.exports = {
  client: {
    lib: {
      css: [
        //'public/lib/bootstrap/dist/css/bootstrap.css',
        //'public/lib/bootstrap/dist/css/bootstrap-theme.css',
        // 'public/lib/authentication/style.css',
        // 'public/lib/authentication/ui.css',
        // 'public/lib/inspinia/animate.css',
        // 'public/lib/landing/style.css',
        // 'public/lib/landing/flaticon.css',
        // 'public/lib/landing/responsive.css',
        // 'public/lib/landing/sbintro.css',

        // '../modules/users/client/css/bootstrap.min.css',
        // '../modules/users/client/css/font-awesome.min.css',
        // '../modules/users/client/css/animate.min.css',
        // '../modules/users/client/css/jquery.steps.css',
        // '../modules/users/client/css/chosen/chosen.css',
        // '../modules/users/client/css/style.min.css',
        // '../modules/users/client/css/styles-core.css',
        // '../modules/users/client/css/styles-core-responsive.css',
        // '../modules/users/client/css/jquery-ui.css',
        // '../modules/users/client/css/tooltipster.css',
        // '../modules/users/client/css/alerts/sweetalert.css',

       // 'public/lib/authentication/style.css',
       // 'public/lib/authentication/ui.css',

      ],
      js: [
        
        'public/lib/angular/angular.js',
        'public/lib/angular-resource/angular-resource.js',
        'public/lib/angular-animate/angular-animate.js',
        'public/lib/angular-messages/angular-messages.js',
        'public/lib/angular-ui-router/release/angular-ui-router.js',
        'public/lib/angular-ui-utils/ui-utils.js',
        'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
        'public/lib/angular-file-upload/angular-file-upload.js',
        'public/lib/owasp-password-strength-test/owasp-password-strength-test.js',

        'public/lib/landingjs/jquery.min.js',
        //'public/lib/landingjs/jquery.easings.min.js',
        //'public/lib/landingjs/jquery.fullPage.js',
        //'public/lib/landingjs/cbpFWTabs.js',
        //'public/lib/landingjs/jquery.sidr.min.js',
        //'public/lib/landingjs/scripts.js',
        //'public/lib/landingjs/zoomcharts.js',
        //'public/lib/landingjs/indexpagedemo.js',
        'public/lib/oclazyload/dist/ocLazyLoad.min.js'

      ],
      tests: ['public/lib/angular-mocks/angular-mocks.js']
    },
    css: [
      'modules/*/client/css/*.css'
    ],
    less: [
      'modules/*/client/less/*.less'
    ],
    sass: [
      'modules/*/client/scss/*.scss'
    ],
    js: [
      'modules/core/client/app/config.js',
      'modules/core/client/app/init.js',
      'modules/*/client/*.js',
      'modules/*/client/**/*.js'
    ],
    img: [
      'modules/**/*/img/**/*.jpg',
      'modules/**/*/img/**/*.png',
      'modules/**/*/img/**/*.gif',
      'modules/**/*/img/**/*.svg'
    ],
    views: ['modules/*/client/views/**/*.html'],
    templates: ['build/templates.js']
  },
  server: {
    gruntConfig: ['gruntfile.js'],
    gulpConfig: ['gulpfile.js'],
    allJS: ['server.js', 'config/**/*.js', 'modules/*/server/**/*.js'],
    models: 'modules/*/server/models/**/*.js',
    routes: ['modules/!(core)/server/routes/**/*.js', 'modules/core/server/routes/**/*.js'],
    sockets: 'modules/*/server/sockets/**/*.js',
    config: ['modules/*/server/config/*.js'],
    policies: 'modules/*/server/policies/*.js',
    views: ['modules/*/server/views/*.html']
  }
};
