  $(document).ready(function() {
    
    setTimeout(function () {

        (function() {
            [].slice.call(document.querySelectorAll('.tabs')).forEach(function(el) {
                new CBPFWTabs(el);
            });
        })();
        $('#main-nav').sidr();
        $('#fullpage').fullpage({
            'verticalCentered': true,
            'easing': 'easeInOutCirc',
            'css3': false,
            'scrollingSpeed': 900,
            'slidesNavigation': true,
            'slidesNavPosition': 'bottom',
            'easingcss3': 'ease',
            'navigation': true,
            'anchors': ['Home', 'Features', 'About', 'Video', 'Clients', 'Screenshots', 'Pricing', 'Download', 'Contact'],
            'navigationPosition': 'left'
        });
        $('.screenshots-content, .clients-content').css('height', $(window).height());

    // CONTACT FORM

 
       $(document).mouseup(function (e) {
            if ($(".sidr-open ")[0]){
            var container = $("#sidr");

            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
              $(".sidr-open #main-nav").click();
            }}
        });
            
             $('#success').hide();
             $('#error').hide();
             var token = getCookie('XSRF-TOKEN');
             $('#_csrf').val(token);
            $('#submit').click(function(){
             $('#submit').val('Sending..');
            $.post("/api/contact", $("#contact-form").serialize(),  function(response) {
                $('#success').fadeIn().html(response);
                $('#success').delay(2000).fadeOut();
                $('#submit').val('Send');
                $("#Name").val('');
                $("#Email").val('');
                $("#Phone").val('');
                $("#Subject").val('');
                $("#Message").val('');
            }).error(function (error, xhr) {
                //console.log('Error ', error.responseText, 'xhr ==>', xhr);
                var err = JSON.parse(error.responseText);
                $('#error').fadeIn().html(err.msg);
                $('#error').delay(2000).fadeOut();
                $('#submit').val('Send');
            });
            return false;
             
            });
 

    },2000);

  });

    function getCookie(name) {
        var re = new RegExp(name + "=([^;]+)");
        var value = re.exec(document.cookie);
        return (value != null) ? unescape(value[1]) : null;
    }

  jQuery(window).load(function() {
    
    setTimeout(function () {
        jQuery('#preloader').fadeOut();
    },2000);

  });
